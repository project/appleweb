<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
<title><?php print $head_title ?></title>
<?php print $head ?><?php print $styles ?><?php print $scripts ?>
<!--[if lt IE 7]>
      <?php print phptemplate_get_ie_styles(); ?>
    <![endif]-->
</head>
<body<?php print phptemplate_body_class($left, $right); ?>>
<div id="main_header">
   <div id="header"> <?php print $toplinks ?> <?php print $topright ?>
      <?php
          // Prepare header
          $site_fields = array();
          if ($site_name) {
            $site_fields[] = check_plain($site_name);
          }
          if ($site_slogan) {
            $site_fields[] = check_plain($site_slogan);
          }
          $site_title = implode(' ', $site_fields);
          if ($site_fields) {
            $site_fields[0] = '<span>'. $site_fields[0] .'</span>';
          }
          $site_html = implode(' ', $site_fields);

          if ($logo || $site_title) {
            print '<h1><a href="'. check_url($front_page) .'" title="'. $site_title .'">';
            if ($logo) {
              print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
            }
            print $site_html .'</a></h1>';
          }
        ?>
      <?php print $header ?>
      <?php if (isset($primary_links)) : ?>
      <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
      <?php endif; ?>
   </div>
</div>
<div id="main_body">
   <div id="body">
   <div class="contentcontainer">
      <div id="leftsidebar"> <?php print $left ?> </div>
      <!--leftsidebar end -->
      <div class="maincontent"><?php print $breadcrumb; ?>
         <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
         <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
         <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
         <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
         <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
         <?php if ($show_messages && $messages): print $messages; endif; ?>
         <?php print $help; ?> <?php print $content ?>
      </div>
       <!--maincontent end -->
      <div id="rightsidebar"> <?php print $right ?> </div>
      <!--rightsidebar end -->
      </div>
      <div id="contentbottom"> <?php print $contentbottom ?> </div>
      <!--contentbottom end-->
   </div>
</div>
<div id="main_footer">
   <div id="footer">
      <!--footer navigation start -->
      <?php if (isset($primary_links)) : ?>
      <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
      <?php endif; ?>
      <?php print $footer; ?>
      <p><?php print $footer_message ?></p>
   </div>
   <!--footer navigation end -->
</div>
<?php print $closure ?>
</body>
</html>
